# Intro

generic json external dns operator.
**ALPHA** 

## pre requisite 

One machine with openshift or kubernetes

### Ubuntu
Follow the install steps on https://kubernetes.io/docs/setup/independent/install-kubeadm/

```
apt update
apt install bash-completion docker.io golang go-dep jq kubelet kubeadm kubectl && apt-mark hold kubelet kubeadm kubectl kubernetes-cni
systemctl enable docker.service
```

### Centos/RHEL

```
yum install golang bash-completion git docker
## add --insecure-registry "172.30.0.0/16" into OPTIONS= line
vi /etc/sysconfig/docker
systemctl enable docker.service
systemctl start docker
```

## Build
```
mkdir .git/
git config credential.helper store
mkdir -p /root/go/bin
export PATH=/root/go/bin:$PATH
export GOPATH="/root/go"
# only for centos
# curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh

curl -vLO https://github.com/openshift/origin/releases/download/v3.10.0-rc.0/openshift-origin-server-v3.10.0-rc.0-c20e215-linux-64bit.tar.gz
tar xfvz openshift-origin-server-v3.10.0-rc.0-c20e215-linux-64bit.tar.gz

mkdir -p /root/go/src/github.com/operator-framework/
cd /root/go/src/github.com/operator-framework/
git clone https://github.com/operator-framework/operator-sdk
cd operator-sdk
git checkout master
make dep
make install
cd ../../..
mkdir -p gitlab.com/aleks001/
cd gitlab.com/aleks001/
git clone https://gitlab.com/aleks001/generic-dns-operator.git
cd generic-dns-operator/
dep ensure
go get github.com/spf13/viper
go get github.com/sirupsen/logrus
export IMAGE=me2digital/test-operator:v0.0.1
operator-sdk build $IMAGE
docker login
docker push $IMAGE
```

## Deploy
Now choose openshift or kubernetes

```
source <(oc completion bash)
oc create -f deploy/create-namespace.yaml
oc -n external-dns create -f deploy/rbac.yaml
oc -n external-dns create -f deploy/crd.yaml
oc -n external-dns create -f deploy/cr.yaml
oc -n external-dns create -f deploy/operator.yaml
#oc -n external-dns patch deployments generic-dns-operator --type='json' -p='[{"op": "add","path": "/spec/template/spec/containers/0/env", "value":[{"name":"WATCH_NAMESPACE","valueFrom":{"fieldRef":{"fieldPath":"metadata.namespace"}}},{"name":"OPERATOR_NAME","value":"generic-dns-operator"}]}]'
oc -n external-dns set env deployments generic-dns-operator \
  OPERATOR_NAME=generic-dns-operator \
  EXTDNS_CNAMEDOMAIN=my.cname.domain \
  EXTDNS_TARGETNAME=THE_VIP_OF_THE_ROUTER \
  EXTDNS_TARGETDOMAIN=THE_TARGET_DOMAIN \
  EXTDNS_TARGETURL=THE_REST_ENDPOIN_URL
```
```
source <(kubectl completion bash)
kubectl create -f deploy/create-namespace.yaml
kubectl -n external-dns create -f deploy/rbac.yaml
kubectl -n external-dns create -f deploy/crd.yaml
kubectl -n external-dns create -f deploy/cr.yaml
kubectl -n external-dns create -f deploy/operator.yaml
#kubectl -n external-dns patch deployments generic-dns-operator --type='json' -p='[{"op": "add","path": "/spec/template/spec/containers/0/env", "value":[{"name":"WATCH_NAMESPACE","valueFrom":{"fieldRef":{"fieldPath":"metadata.namespace"}}},{"name":"OPERATOR_NAME","value":"generic-dns-operator"}]}]'
kubectl -n external-dns set env deployments generic-dns-operator \
  OPERATOR_NAME=generic-dns-operator \
  EXTDNS_CNAMEDOMAIN=my.cname.domain \
  EXTDNS_TARGETNAME=THE_VIP_OF_THE_ROUTER \
  EXTDNS_TARGETDOMAIN=THE_TARGET_DOMAIN \
  EXTDNS_TARGETURL=THE_REST_ENDPOIN_URL
```

```
systemctl stop docker

echo '{
  "insecure-registries": ["172.30.0.0/16"]
}' > /etc/docker/daemon.json

systemctl start docker
```

```
export PATH=/root/openshift-origin-server-v3.9.0-191fece-linux-64bit:$PATH
oc cluster up --public-hostname=178.128.207.183
source <(oc completion bash)

oc project gen-dns-ops
```
