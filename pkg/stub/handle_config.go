package stub

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"os"
)

func SetLogLevel(){
	loglevel := os.Getenv("LOGLEVEL")

	if loglevel != "" {
		switch loglevel {
		case "DEBUG":
			log.SetLevel(log.DebugLevel)
		case "WARN":
			log.SetLevel(log.WarnLevel)
		default:
			log.SetLevel(log.InfoLevel)
		}
	} else {
		log.SetLevel(log.InfoLevel)
	}

	log.SetOutput(os.Stdout)
}

func ReadConfig(envprefix string, cfg *viper.Viper) bool {

	SetLogLevel()
	log.WithFields(log.Fields{"config": "reader"}).Info("ReadConfig start")

	log.WithFields(log.Fields{"envprefix": envprefix}).Debug("Got following values")

	cfg.AutomaticEnv()
	log.Debug("Success Autoenv")
	cfg.SetEnvPrefix(envprefix)
	log.Debug("Success envPref")

	myerror := cfg.Get("cnamedomain")
	if myerror == nil {
		log.Error("Env cnamedomain was not found")
		return false
	}
	log.Debug("Success set cmane")

	myerror = cfg.Get("targetname")
	if myerror == nil {
		log.Error("Env targetname was not found")
		return false
	}
	log.Debug("Success set tname")

	myerror = cfg.Get("targetdomain")
	if myerror == nil {
		log.Error("Env targetdomain was not found")
		return false
	}
	log.Debug("Success set tdomain")

	myerror = cfg.Get("targeturl")
	if myerror == nil {
		log.Error("Env targeturl was not found")
		return false
	}
	log.Debug("Success set turl")

	log.WithFields(log.Fields{"cnamedomain":  cfg.Get("cnamedomain")} ).Info("ReadConfig():Cname Domain")
	log.WithFields(log.Fields{"targetname":   cfg.Get("targetname")}  ).Info("ReadConfig():Target Name")
	log.WithFields(log.Fields{"targetdomain": cfg.Get("targetdomain")}).Info("ReadConfig():Target Domain")
	log.WithFields(log.Fields{"targeturl":    cfg.Get("targeturl")}   ).Info("ReadConfig():Target URL")

	return true
}

func main() {

	SetLogLevel()

	config := viper.New()
	err := ReadConfig("EXTDNS", config)
	if err {
		fmt.Printf("%s", err)
	}
}
